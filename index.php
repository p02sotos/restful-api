<?php

require 'vendor/autoload.php';
require_once 'vendor/propel/propel1/runtime/lib/Propel.php';
Propel::init("build/conf/incidencias-conf.php");
set_include_path("build/classes" . PATH_SEPARATOR . get_include_path());

$app = new \Slim\Slim();
$app->config(array(
    'debug' => true
));

$app->get('/', function () use($app) {
    echo "Indice";
})->name('home');

$app->get('/incidencia', function () use($app) {
    $incidencias = IncidenciasQuery::create()->find();
    //$incidenciasToSend = array("incidencias"=>$incidencias->toJSON(false));
    if ($incidencias->count() > 0) {
        //echo json_encode($incidenciasToSend);       
        $incArray = $incidencias->toArray();
        $newArray = array();
        foreach ($incArray as $key => $value) {
            $value = array_change_key_case($value, CASE_LOWER);
            array_push($newArray, $value);
        }
        $resp = array("incidencias" => $newArray);


        echo json_encode($resp);
    } else {
        echo "sin datos";
    }
})->name('incidenciasAPI');
$app->get('/incidencia/:id', function ($id) use($app) {
    $incidencia = IncidenciasQuery::create()->findPk($id);
    if (isset($incidencia)) {
        echo $incidencia->toJSON(false);
    } else {
        echo 'no encontrado';
    }
})->name('obtenerIncidenciaAPI');
$app->put('/incidencia/:id', function ($id) use($app) {
    $request = $app->request();
    $incid = json_decode($request->getBody(), true);
    $inc = $incid['incidencia'];
    $incidencia = IncidenciasQuery::create()->findPk($id);
    $incidencia->setMotivo($inc['motivo']);
    $incidencia->setComunidad($inc['comunidad']);
    $incidencia->setResumen($inc['resumen']);
    $incidencia->setDescripcion($inc['descripcion']);
    $incidencia->setFechaEntrada($inc['fechaentrada']);
    $incidencia->setHoraEntrada($inc['horaentrada']);
    $incidencia->setFechaResolucion($inc['fecharesolucion']);
    $incidencia->setRealizada($inc['realizada']);
    $incidencia->setUrgencia($inc['urgencia']);
    $incidencia->save();
    //echo $incidencia->toJSON();
    //$redireccion = $app->urlFor('editarIncidencia', array('id'=>$incidencia->getId()));
    //$app->redirect($redireccion);
})->name('editarIncidenciaAPI');
$app->post('/incidencia', function () use($app) {
    $request = $app->request();
    $incid = json_decode($request->getBody(), true);
    $inc = $incid['incidencia'];
    $incidencia = new Incidencias();
    $incidencia->setMotivo($inc['motivo']);
    $incidencia->setComunidad($inc['comunidad']);
    $incidencia->setResumen($inc['resumen']);
    $incidencia->setDescripcion($inc['descripcion']);
    $incidencia->setFechaEntrada($inc['fechaentrada']);
    $incidencia->setHoraEntrada($inc['horaentrada']);
    $incidencia->setFechaResolucion($inc['fecharesolucion']);
    $incidencia->setRealizada($inc['realizada']);
    $incidencia->setUrgencia($inc['urgencia']);
    $incidencia->save();
    
});
$app->delete('/incidencias/:id', function ($id) use($app) {
    $incidencia = IncidenciasQuery::create()->findPk($id)->delete();
    echo $incidencia->isDeleted();
})->name('borrarIncidenciaAPI');



$app->run();


