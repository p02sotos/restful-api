
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- incidencias
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `incidencias`;

CREATE TABLE `incidencias`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `origen` VARCHAR(1000),
    `motivo` VARCHAR(1000),
    `comunidad` VARCHAR(1000),
    `resumen` VARCHAR(1000),
    `descripcion` TEXT,
    `fecha_entrada` DATE,
    `hora_entrada` TIME,
    `fecha_resolucion` DATE,
    `realizada` TINYINT(1),
    `urgencia` VARCHAR(255),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
