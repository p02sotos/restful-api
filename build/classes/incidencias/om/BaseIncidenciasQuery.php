<?php


/**
 * Base class that represents a query for the 'incidencias' table.
 *
 * 
 *
 * @method IncidenciasQuery orderById($order = Criteria::ASC) Order by the id column
 * @method IncidenciasQuery orderByOrigen($order = Criteria::ASC) Order by the origen column
 * @method IncidenciasQuery orderByMotivo($order = Criteria::ASC) Order by the motivo column
 * @method IncidenciasQuery orderByComunidad($order = Criteria::ASC) Order by the comunidad column
 * @method IncidenciasQuery orderByResumen($order = Criteria::ASC) Order by the resumen column
 * @method IncidenciasQuery orderByDescripcion($order = Criteria::ASC) Order by the descripcion column
 * @method IncidenciasQuery orderByFechaEntrada($order = Criteria::ASC) Order by the fecha_entrada column
 * @method IncidenciasQuery orderByHoraEntrada($order = Criteria::ASC) Order by the hora_entrada column
 * @method IncidenciasQuery orderByFechaResolucion($order = Criteria::ASC) Order by the fecha_resolucion column
 * @method IncidenciasQuery orderByRealizada($order = Criteria::ASC) Order by the realizada column
 * @method IncidenciasQuery orderByUrgencia($order = Criteria::ASC) Order by the urgencia column
 *
 * @method IncidenciasQuery groupById() Group by the id column
 * @method IncidenciasQuery groupByOrigen() Group by the origen column
 * @method IncidenciasQuery groupByMotivo() Group by the motivo column
 * @method IncidenciasQuery groupByComunidad() Group by the comunidad column
 * @method IncidenciasQuery groupByResumen() Group by the resumen column
 * @method IncidenciasQuery groupByDescripcion() Group by the descripcion column
 * @method IncidenciasQuery groupByFechaEntrada() Group by the fecha_entrada column
 * @method IncidenciasQuery groupByHoraEntrada() Group by the hora_entrada column
 * @method IncidenciasQuery groupByFechaResolucion() Group by the fecha_resolucion column
 * @method IncidenciasQuery groupByRealizada() Group by the realizada column
 * @method IncidenciasQuery groupByUrgencia() Group by the urgencia column
 *
 * @method IncidenciasQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method IncidenciasQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method IncidenciasQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Incidencias findOne(PropelPDO $con = null) Return the first Incidencias matching the query
 * @method Incidencias findOneOrCreate(PropelPDO $con = null) Return the first Incidencias matching the query, or a new Incidencias object populated from the query conditions when no match is found
 *
 * @method Incidencias findOneByOrigen(string $origen) Return the first Incidencias filtered by the origen column
 * @method Incidencias findOneByMotivo(string $motivo) Return the first Incidencias filtered by the motivo column
 * @method Incidencias findOneByComunidad(string $comunidad) Return the first Incidencias filtered by the comunidad column
 * @method Incidencias findOneByResumen(string $resumen) Return the first Incidencias filtered by the resumen column
 * @method Incidencias findOneByDescripcion(string $descripcion) Return the first Incidencias filtered by the descripcion column
 * @method Incidencias findOneByFechaEntrada(string $fecha_entrada) Return the first Incidencias filtered by the fecha_entrada column
 * @method Incidencias findOneByHoraEntrada(string $hora_entrada) Return the first Incidencias filtered by the hora_entrada column
 * @method Incidencias findOneByFechaResolucion(string $fecha_resolucion) Return the first Incidencias filtered by the fecha_resolucion column
 * @method Incidencias findOneByRealizada(boolean $realizada) Return the first Incidencias filtered by the realizada column
 * @method Incidencias findOneByUrgencia(string $urgencia) Return the first Incidencias filtered by the urgencia column
 *
 * @method array findById(int $id) Return Incidencias objects filtered by the id column
 * @method array findByOrigen(string $origen) Return Incidencias objects filtered by the origen column
 * @method array findByMotivo(string $motivo) Return Incidencias objects filtered by the motivo column
 * @method array findByComunidad(string $comunidad) Return Incidencias objects filtered by the comunidad column
 * @method array findByResumen(string $resumen) Return Incidencias objects filtered by the resumen column
 * @method array findByDescripcion(string $descripcion) Return Incidencias objects filtered by the descripcion column
 * @method array findByFechaEntrada(string $fecha_entrada) Return Incidencias objects filtered by the fecha_entrada column
 * @method array findByHoraEntrada(string $hora_entrada) Return Incidencias objects filtered by the hora_entrada column
 * @method array findByFechaResolucion(string $fecha_resolucion) Return Incidencias objects filtered by the fecha_resolucion column
 * @method array findByRealizada(boolean $realizada) Return Incidencias objects filtered by the realizada column
 * @method array findByUrgencia(string $urgencia) Return Incidencias objects filtered by the urgencia column
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BaseIncidenciasQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseIncidenciasQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'incidencias';
        }
        if (null === $modelName) {
            $modelName = 'Incidencias';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new IncidenciasQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   IncidenciasQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return IncidenciasQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof IncidenciasQuery) {
            return $criteria;
        }
        $query = new IncidenciasQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Incidencias|Incidencias[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = IncidenciasPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(IncidenciasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Incidencias A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Incidencias A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `origen`, `motivo`, `comunidad`, `resumen`, `descripcion`, `fecha_entrada`, `hora_entrada`, `fecha_resolucion`, `realizada`, `urgencia` FROM `incidencias` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Incidencias();
            $obj->hydrate($row);
            IncidenciasPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Incidencias|Incidencias[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Incidencias[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return IncidenciasQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(IncidenciasPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return IncidenciasQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(IncidenciasPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciasQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(IncidenciasPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(IncidenciasPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IncidenciasPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the origen column
     *
     * Example usage:
     * <code>
     * $query->filterByOrigen('fooValue');   // WHERE origen = 'fooValue'
     * $query->filterByOrigen('%fooValue%'); // WHERE origen LIKE '%fooValue%'
     * </code>
     *
     * @param     string $origen The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciasQuery The current query, for fluid interface
     */
    public function filterByOrigen($origen = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($origen)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $origen)) {
                $origen = str_replace('*', '%', $origen);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IncidenciasPeer::ORIGEN, $origen, $comparison);
    }

    /**
     * Filter the query on the motivo column
     *
     * Example usage:
     * <code>
     * $query->filterByMotivo('fooValue');   // WHERE motivo = 'fooValue'
     * $query->filterByMotivo('%fooValue%'); // WHERE motivo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $motivo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciasQuery The current query, for fluid interface
     */
    public function filterByMotivo($motivo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($motivo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $motivo)) {
                $motivo = str_replace('*', '%', $motivo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IncidenciasPeer::MOTIVO, $motivo, $comparison);
    }

    /**
     * Filter the query on the comunidad column
     *
     * Example usage:
     * <code>
     * $query->filterByComunidad('fooValue');   // WHERE comunidad = 'fooValue'
     * $query->filterByComunidad('%fooValue%'); // WHERE comunidad LIKE '%fooValue%'
     * </code>
     *
     * @param     string $comunidad The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciasQuery The current query, for fluid interface
     */
    public function filterByComunidad($comunidad = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($comunidad)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $comunidad)) {
                $comunidad = str_replace('*', '%', $comunidad);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IncidenciasPeer::COMUNIDAD, $comunidad, $comparison);
    }

    /**
     * Filter the query on the resumen column
     *
     * Example usage:
     * <code>
     * $query->filterByResumen('fooValue');   // WHERE resumen = 'fooValue'
     * $query->filterByResumen('%fooValue%'); // WHERE resumen LIKE '%fooValue%'
     * </code>
     *
     * @param     string $resumen The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciasQuery The current query, for fluid interface
     */
    public function filterByResumen($resumen = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($resumen)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $resumen)) {
                $resumen = str_replace('*', '%', $resumen);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IncidenciasPeer::RESUMEN, $resumen, $comparison);
    }

    /**
     * Filter the query on the descripcion column
     *
     * Example usage:
     * <code>
     * $query->filterByDescripcion('fooValue');   // WHERE descripcion = 'fooValue'
     * $query->filterByDescripcion('%fooValue%'); // WHERE descripcion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descripcion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciasQuery The current query, for fluid interface
     */
    public function filterByDescripcion($descripcion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descripcion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $descripcion)) {
                $descripcion = str_replace('*', '%', $descripcion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IncidenciasPeer::DESCRIPCION, $descripcion, $comparison);
    }

    /**
     * Filter the query on the fecha_entrada column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaEntrada('2011-03-14'); // WHERE fecha_entrada = '2011-03-14'
     * $query->filterByFechaEntrada('now'); // WHERE fecha_entrada = '2011-03-14'
     * $query->filterByFechaEntrada(array('max' => 'yesterday')); // WHERE fecha_entrada < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaEntrada The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciasQuery The current query, for fluid interface
     */
    public function filterByFechaEntrada($fechaEntrada = null, $comparison = null)
    {
        if (is_array($fechaEntrada)) {
            $useMinMax = false;
            if (isset($fechaEntrada['min'])) {
                $this->addUsingAlias(IncidenciasPeer::FECHA_ENTRADA, $fechaEntrada['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaEntrada['max'])) {
                $this->addUsingAlias(IncidenciasPeer::FECHA_ENTRADA, $fechaEntrada['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IncidenciasPeer::FECHA_ENTRADA, $fechaEntrada, $comparison);
    }

    /**
     * Filter the query on the hora_entrada column
     *
     * Example usage:
     * <code>
     * $query->filterByHoraEntrada('2011-03-14'); // WHERE hora_entrada = '2011-03-14'
     * $query->filterByHoraEntrada('now'); // WHERE hora_entrada = '2011-03-14'
     * $query->filterByHoraEntrada(array('max' => 'yesterday')); // WHERE hora_entrada < '2011-03-13'
     * </code>
     *
     * @param     mixed $horaEntrada The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciasQuery The current query, for fluid interface
     */
    public function filterByHoraEntrada($horaEntrada = null, $comparison = null)
    {
        if (is_array($horaEntrada)) {
            $useMinMax = false;
            if (isset($horaEntrada['min'])) {
                $this->addUsingAlias(IncidenciasPeer::HORA_ENTRADA, $horaEntrada['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($horaEntrada['max'])) {
                $this->addUsingAlias(IncidenciasPeer::HORA_ENTRADA, $horaEntrada['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IncidenciasPeer::HORA_ENTRADA, $horaEntrada, $comparison);
    }

    /**
     * Filter the query on the fecha_resolucion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaResolucion('2011-03-14'); // WHERE fecha_resolucion = '2011-03-14'
     * $query->filterByFechaResolucion('now'); // WHERE fecha_resolucion = '2011-03-14'
     * $query->filterByFechaResolucion(array('max' => 'yesterday')); // WHERE fecha_resolucion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaResolucion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciasQuery The current query, for fluid interface
     */
    public function filterByFechaResolucion($fechaResolucion = null, $comparison = null)
    {
        if (is_array($fechaResolucion)) {
            $useMinMax = false;
            if (isset($fechaResolucion['min'])) {
                $this->addUsingAlias(IncidenciasPeer::FECHA_RESOLUCION, $fechaResolucion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaResolucion['max'])) {
                $this->addUsingAlias(IncidenciasPeer::FECHA_RESOLUCION, $fechaResolucion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IncidenciasPeer::FECHA_RESOLUCION, $fechaResolucion, $comparison);
    }

    /**
     * Filter the query on the realizada column
     *
     * Example usage:
     * <code>
     * $query->filterByRealizada(true); // WHERE realizada = true
     * $query->filterByRealizada('yes'); // WHERE realizada = true
     * </code>
     *
     * @param     boolean|string $realizada The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciasQuery The current query, for fluid interface
     */
    public function filterByRealizada($realizada = null, $comparison = null)
    {
        if (is_string($realizada)) {
            $realizada = in_array(strtolower($realizada), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(IncidenciasPeer::REALIZADA, $realizada, $comparison);
    }

    /**
     * Filter the query on the urgencia column
     *
     * Example usage:
     * <code>
     * $query->filterByUrgencia('fooValue');   // WHERE urgencia = 'fooValue'
     * $query->filterByUrgencia('%fooValue%'); // WHERE urgencia LIKE '%fooValue%'
     * </code>
     *
     * @param     string $urgencia The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciasQuery The current query, for fluid interface
     */
    public function filterByUrgencia($urgencia = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($urgencia)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $urgencia)) {
                $urgencia = str_replace('*', '%', $urgencia);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IncidenciasPeer::URGENCIA, $urgencia, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Incidencias $incidencias Object to remove from the list of results
     *
     * @return IncidenciasQuery The current query, for fluid interface
     */
    public function prune($incidencias = null)
    {
        if ($incidencias) {
            $this->addUsingAlias(IncidenciasPeer::ID, $incidencias->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
