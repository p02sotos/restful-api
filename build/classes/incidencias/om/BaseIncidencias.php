<?php


/**
 * Base class that represents a row from the 'incidencias' table.
 *
 * 
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BaseIncidencias extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'IncidenciasPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        IncidenciasPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the origen field.
     * @var        string
     */
    protected $origen;

    /**
     * The value for the motivo field.
     * @var        string
     */
    protected $motivo;

    /**
     * The value for the comunidad field.
     * @var        string
     */
    protected $comunidad;

    /**
     * The value for the resumen field.
     * @var        string
     */
    protected $resumen;

    /**
     * The value for the descripcion field.
     * @var        string
     */
    protected $descripcion;

    /**
     * The value for the fecha_entrada field.
     * @var        string
     */
    protected $fecha_entrada;

    /**
     * The value for the hora_entrada field.
     * @var        string
     */
    protected $hora_entrada;

    /**
     * The value for the fecha_resolucion field.
     * @var        string
     */
    protected $fecha_resolucion;

    /**
     * The value for the realizada field.
     * @var        boolean
     */
    protected $realizada;

    /**
     * The value for the urgencia field.
     * @var        string
     */
    protected $urgencia;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [origen] column value.
     * 
     * @return string
     */
    public function getOrigen()
    {

        return $this->origen;
    }

    /**
     * Get the [motivo] column value.
     * 
     * @return string
     */
    public function getMotivo()
    {

        return $this->motivo;
    }

    /**
     * Get the [comunidad] column value.
     * 
     * @return string
     */
    public function getComunidad()
    {

        return $this->comunidad;
    }

    /**
     * Get the [resumen] column value.
     * 
     * @return string
     */
    public function getResumen()
    {

        return $this->resumen;
    }

    /**
     * Get the [descripcion] column value.
     * 
     * @return string
     */
    public function getDescripcion()
    {

        return $this->descripcion;
    }

    /**
     * Get the [optionally formatted] temporal [fecha_entrada] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaEntrada($format = '%x')
    {
        if ($this->fecha_entrada === null) {
            return null;
        }

        if ($this->fecha_entrada === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_entrada);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_entrada, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [hora_entrada] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getHoraEntrada($format = '%X')
    {
        if ($this->hora_entrada === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->hora_entrada);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->hora_entrada, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [fecha_resolucion] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaResolucion($format = '%x')
    {
        if ($this->fecha_resolucion === null) {
            return null;
        }

        if ($this->fecha_resolucion === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_resolucion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_resolucion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [realizada] column value.
     * 
     * @return boolean
     */
    public function getRealizada()
    {

        return $this->realizada;
    }

    /**
     * Get the [urgencia] column value.
     * 
     * @return string
     */
    public function getUrgencia()
    {

        return $this->urgencia;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param  int $v new value
     * @return Incidencias The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = IncidenciasPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [origen] column.
     * 
     * @param  string $v new value
     * @return Incidencias The current object (for fluent API support)
     */
    public function setOrigen($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->origen !== $v) {
            $this->origen = $v;
            $this->modifiedColumns[] = IncidenciasPeer::ORIGEN;
        }


        return $this;
    } // setOrigen()

    /**
     * Set the value of [motivo] column.
     * 
     * @param  string $v new value
     * @return Incidencias The current object (for fluent API support)
     */
    public function setMotivo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->motivo !== $v) {
            $this->motivo = $v;
            $this->modifiedColumns[] = IncidenciasPeer::MOTIVO;
        }


        return $this;
    } // setMotivo()

    /**
     * Set the value of [comunidad] column.
     * 
     * @param  string $v new value
     * @return Incidencias The current object (for fluent API support)
     */
    public function setComunidad($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->comunidad !== $v) {
            $this->comunidad = $v;
            $this->modifiedColumns[] = IncidenciasPeer::COMUNIDAD;
        }


        return $this;
    } // setComunidad()

    /**
     * Set the value of [resumen] column.
     * 
     * @param  string $v new value
     * @return Incidencias The current object (for fluent API support)
     */
    public function setResumen($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->resumen !== $v) {
            $this->resumen = $v;
            $this->modifiedColumns[] = IncidenciasPeer::RESUMEN;
        }


        return $this;
    } // setResumen()

    /**
     * Set the value of [descripcion] column.
     * 
     * @param  string $v new value
     * @return Incidencias The current object (for fluent API support)
     */
    public function setDescripcion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descripcion !== $v) {
            $this->descripcion = $v;
            $this->modifiedColumns[] = IncidenciasPeer::DESCRIPCION;
        }


        return $this;
    } // setDescripcion()

    /**
     * Sets the value of [fecha_entrada] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Incidencias The current object (for fluent API support)
     */
    public function setFechaEntrada($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_entrada !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_entrada !== null && $tmpDt = new DateTime($this->fecha_entrada)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_entrada = $newDateAsString;
                $this->modifiedColumns[] = IncidenciasPeer::FECHA_ENTRADA;
            }
        } // if either are not null


        return $this;
    } // setFechaEntrada()

    /**
     * Sets the value of [hora_entrada] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Incidencias The current object (for fluent API support)
     */
    public function setHoraEntrada($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->hora_entrada !== null || $dt !== null) {
            $currentDateAsString = ($this->hora_entrada !== null && $tmpDt = new DateTime($this->hora_entrada)) ? $tmpDt->format('H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->hora_entrada = $newDateAsString;
                $this->modifiedColumns[] = IncidenciasPeer::HORA_ENTRADA;
            }
        } // if either are not null


        return $this;
    } // setHoraEntrada()

    /**
     * Sets the value of [fecha_resolucion] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Incidencias The current object (for fluent API support)
     */
    public function setFechaResolucion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_resolucion !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_resolucion !== null && $tmpDt = new DateTime($this->fecha_resolucion)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_resolucion = $newDateAsString;
                $this->modifiedColumns[] = IncidenciasPeer::FECHA_RESOLUCION;
            }
        } // if either are not null


        return $this;
    } // setFechaResolucion()

    /**
     * Sets the value of the [realizada] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param boolean|integer|string $v The new value
     * @return Incidencias The current object (for fluent API support)
     */
    public function setRealizada($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->realizada !== $v) {
            $this->realizada = $v;
            $this->modifiedColumns[] = IncidenciasPeer::REALIZADA;
        }


        return $this;
    } // setRealizada()

    /**
     * Set the value of [urgencia] column.
     * 
     * @param  string $v new value
     * @return Incidencias The current object (for fluent API support)
     */
    public function setUrgencia($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->urgencia !== $v) {
            $this->urgencia = $v;
            $this->modifiedColumns[] = IncidenciasPeer::URGENCIA;
        }


        return $this;
    } // setUrgencia()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->origen = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->motivo = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->comunidad = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->resumen = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->descripcion = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->fecha_entrada = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->hora_entrada = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->fecha_resolucion = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->realizada = ($row[$startcol + 9] !== null) ? (boolean) $row[$startcol + 9] : null;
            $this->urgencia = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 11; // 11 = IncidenciasPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Incidencias object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(IncidenciasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = IncidenciasPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(IncidenciasPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = IncidenciasQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(IncidenciasPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                IncidenciasPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = IncidenciasPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . IncidenciasPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(IncidenciasPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(IncidenciasPeer::ORIGEN)) {
            $modifiedColumns[':p' . $index++]  = '`origen`';
        }
        if ($this->isColumnModified(IncidenciasPeer::MOTIVO)) {
            $modifiedColumns[':p' . $index++]  = '`motivo`';
        }
        if ($this->isColumnModified(IncidenciasPeer::COMUNIDAD)) {
            $modifiedColumns[':p' . $index++]  = '`comunidad`';
        }
        if ($this->isColumnModified(IncidenciasPeer::RESUMEN)) {
            $modifiedColumns[':p' . $index++]  = '`resumen`';
        }
        if ($this->isColumnModified(IncidenciasPeer::DESCRIPCION)) {
            $modifiedColumns[':p' . $index++]  = '`descripcion`';
        }
        if ($this->isColumnModified(IncidenciasPeer::FECHA_ENTRADA)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_entrada`';
        }
        if ($this->isColumnModified(IncidenciasPeer::HORA_ENTRADA)) {
            $modifiedColumns[':p' . $index++]  = '`hora_entrada`';
        }
        if ($this->isColumnModified(IncidenciasPeer::FECHA_RESOLUCION)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_resolucion`';
        }
        if ($this->isColumnModified(IncidenciasPeer::REALIZADA)) {
            $modifiedColumns[':p' . $index++]  = '`realizada`';
        }
        if ($this->isColumnModified(IncidenciasPeer::URGENCIA)) {
            $modifiedColumns[':p' . $index++]  = '`urgencia`';
        }

        $sql = sprintf(
            'INSERT INTO `incidencias` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`origen`':						
                        $stmt->bindValue($identifier, $this->origen, PDO::PARAM_STR);
                        break;
                    case '`motivo`':						
                        $stmt->bindValue($identifier, $this->motivo, PDO::PARAM_STR);
                        break;
                    case '`comunidad`':						
                        $stmt->bindValue($identifier, $this->comunidad, PDO::PARAM_STR);
                        break;
                    case '`resumen`':						
                        $stmt->bindValue($identifier, $this->resumen, PDO::PARAM_STR);
                        break;
                    case '`descripcion`':						
                        $stmt->bindValue($identifier, $this->descripcion, PDO::PARAM_STR);
                        break;
                    case '`fecha_entrada`':						
                        $stmt->bindValue($identifier, $this->fecha_entrada, PDO::PARAM_STR);
                        break;
                    case '`hora_entrada`':						
                        $stmt->bindValue($identifier, $this->hora_entrada, PDO::PARAM_STR);
                        break;
                    case '`fecha_resolucion`':						
                        $stmt->bindValue($identifier, $this->fecha_resolucion, PDO::PARAM_STR);
                        break;
                    case '`realizada`':
                        $stmt->bindValue($identifier, (int) $this->realizada, PDO::PARAM_INT);
                        break;
                    case '`urgencia`':						
                        $stmt->bindValue($identifier, $this->urgencia, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = IncidenciasPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = IncidenciasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getOrigen();
                break;
            case 2:
                return $this->getMotivo();
                break;
            case 3:
                return $this->getComunidad();
                break;
            case 4:
                return $this->getResumen();
                break;
            case 5:
                return $this->getDescripcion();
                break;
            case 6:
                return $this->getFechaEntrada();
                break;
            case 7:
                return $this->getHoraEntrada();
                break;
            case 8:
                return $this->getFechaResolucion();
                break;
            case 9:
                return $this->getRealizada();
                break;
            case 10:
                return $this->getUrgencia();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['Incidencias'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Incidencias'][$this->getPrimaryKey()] = true;
        $keys = IncidenciasPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getOrigen(),
            $keys[2] => $this->getMotivo(),
            $keys[3] => $this->getComunidad(),
            $keys[4] => $this->getResumen(),
            $keys[5] => $this->getDescripcion(),
            $keys[6] => $this->getFechaEntrada(),
            $keys[7] => $this->getHoraEntrada(),
            $keys[8] => $this->getFechaResolucion(),
            $keys[9] => $this->getRealizada(),
            $keys[10] => $this->getUrgencia(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }
        

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = IncidenciasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setOrigen($value);
                break;
            case 2:
                $this->setMotivo($value);
                break;
            case 3:
                $this->setComunidad($value);
                break;
            case 4:
                $this->setResumen($value);
                break;
            case 5:
                $this->setDescripcion($value);
                break;
            case 6:
                $this->setFechaEntrada($value);
                break;
            case 7:
                $this->setHoraEntrada($value);
                break;
            case 8:
                $this->setFechaResolucion($value);
                break;
            case 9:
                $this->setRealizada($value);
                break;
            case 10:
                $this->setUrgencia($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = IncidenciasPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setOrigen($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setMotivo($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setComunidad($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setResumen($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setDescripcion($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setFechaEntrada($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setHoraEntrada($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setFechaResolucion($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setRealizada($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setUrgencia($arr[$keys[10]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(IncidenciasPeer::DATABASE_NAME);

        if ($this->isColumnModified(IncidenciasPeer::ID)) $criteria->add(IncidenciasPeer::ID, $this->id);
        if ($this->isColumnModified(IncidenciasPeer::ORIGEN)) $criteria->add(IncidenciasPeer::ORIGEN, $this->origen);
        if ($this->isColumnModified(IncidenciasPeer::MOTIVO)) $criteria->add(IncidenciasPeer::MOTIVO, $this->motivo);
        if ($this->isColumnModified(IncidenciasPeer::COMUNIDAD)) $criteria->add(IncidenciasPeer::COMUNIDAD, $this->comunidad);
        if ($this->isColumnModified(IncidenciasPeer::RESUMEN)) $criteria->add(IncidenciasPeer::RESUMEN, $this->resumen);
        if ($this->isColumnModified(IncidenciasPeer::DESCRIPCION)) $criteria->add(IncidenciasPeer::DESCRIPCION, $this->descripcion);
        if ($this->isColumnModified(IncidenciasPeer::FECHA_ENTRADA)) $criteria->add(IncidenciasPeer::FECHA_ENTRADA, $this->fecha_entrada);
        if ($this->isColumnModified(IncidenciasPeer::HORA_ENTRADA)) $criteria->add(IncidenciasPeer::HORA_ENTRADA, $this->hora_entrada);
        if ($this->isColumnModified(IncidenciasPeer::FECHA_RESOLUCION)) $criteria->add(IncidenciasPeer::FECHA_RESOLUCION, $this->fecha_resolucion);
        if ($this->isColumnModified(IncidenciasPeer::REALIZADA)) $criteria->add(IncidenciasPeer::REALIZADA, $this->realizada);
        if ($this->isColumnModified(IncidenciasPeer::URGENCIA)) $criteria->add(IncidenciasPeer::URGENCIA, $this->urgencia);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(IncidenciasPeer::DATABASE_NAME);
        $criteria->add(IncidenciasPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Incidencias (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setOrigen($this->getOrigen());
        $copyObj->setMotivo($this->getMotivo());
        $copyObj->setComunidad($this->getComunidad());
        $copyObj->setResumen($this->getResumen());
        $copyObj->setDescripcion($this->getDescripcion());
        $copyObj->setFechaEntrada($this->getFechaEntrada());
        $copyObj->setHoraEntrada($this->getHoraEntrada());
        $copyObj->setFechaResolucion($this->getFechaResolucion());
        $copyObj->setRealizada($this->getRealizada());
        $copyObj->setUrgencia($this->getUrgencia());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Incidencias Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return IncidenciasPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new IncidenciasPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->origen = null;
        $this->motivo = null;
        $this->comunidad = null;
        $this->resumen = null;
        $this->descripcion = null;
        $this->fecha_entrada = null;
        $this->hora_entrada = null;
        $this->fecha_resolucion = null;
        $this->realizada = null;
        $this->urgencia = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(IncidenciasPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
