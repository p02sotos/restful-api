<?php



/**
 * This class defines the structure of the 'incidencias' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.incidencias.map
 */
class IncidenciasTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'incidencias.map.IncidenciasTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('incidencias');
        $this->setPhpName('Incidencias');
        $this->setClassname('Incidencias');
        $this->setPackage('incidencias');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('origen', 'Origen', 'VARCHAR', false, 1000, null);
        $this->addColumn('motivo', 'Motivo', 'VARCHAR', false, 1000, null);
        $this->addColumn('comunidad', 'Comunidad', 'VARCHAR', false, 1000, null);
        $this->addColumn('resumen', 'Resumen', 'VARCHAR', false, 1000, null);
        $this->addColumn('descripcion', 'Descripcion', 'LONGVARCHAR', false, null, null);
        $this->addColumn('fecha_entrada', 'FechaEntrada', 'DATE', false, null, null);
        $this->addColumn('hora_entrada', 'HoraEntrada', 'TIME', false, null, null);
        $this->addColumn('fecha_resolucion', 'FechaResolucion', 'DATE', false, null, null);
        $this->addColumn('realizada', 'Realizada', 'BOOLEAN', false, 1, null);
        $this->addColumn('urgencia', 'Urgencia', 'VARCHAR', false, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // IncidenciasTableMap
